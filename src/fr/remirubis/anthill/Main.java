package fr.remirubis.anthill;

import java.io.FilterInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import fr.remirubis.anthill.models.AntTypes;
import fr.remirubis.anthill.models.Anthill;

public class Main {

	private static Map<AntTypes, Integer> anthillPopulation = new HashMap<AntTypes, Integer>();
	
	/*
	 * 
   * If you have problems with the colors and you see ANSI code, like: [0;31m / [0m
   * 
   * Please install the following extension "Ansi Escape Code"
   * find this plugin on Help > MarketPlace > Ansi Escape Code
   * 
   */

	public static void main(String[] args) {
		
		generateInputs(new ArrayList<AntTypes>(Arrays.asList(AntTypes.values())));
		new Anthill(anthillPopulation);
		
	}
	
	/**
	 * 
	 * Generate all inputs for user and get all data
	 * 
	 * @param antTypes
	 * @return int
	 */
	public static int generateInputs(List<AntTypes> antTypes) {
		AntTypes type = antTypes.get(0);
		boolean error = true;
		int inputNumber = type == AntTypes.QUEEN ? 1 : random(type.getMaxRandomValue());

    String input = "";
    try (Scanner scan = new Scanner(new FilterInputStream(System.in){public void close(){}})) {
        while (error) {
        	System.out.print("Enter number of " + type.getColor() + type.getName() + "s" + Color.RESET + " : ");
        	
        	if(!scan.hasNextLine() || (input = scan.nextLine().toLowerCase()).length() == 0) {
        		error = false;
        	} else {        		
        		boolean isNb = isNumber(input);
        		if (isNb) {
        			error = false;
        			inputNumber = Integer.parseInt(input);
        		}
        	}
        }
        antTypes.remove(0);
        anthillPopulation.put(type, inputNumber);
        if (antTypes.size() > 0) generateInputs(antTypes);
    }
		return inputNumber;
	}
	
	/**
	 * 
	 * Get a random number between 1 and max param
	 * 
	 * @param max
	 * @return int
	 */
	public static int random(int max) {
		Random rand = new Random();
		return rand.nextInt(max + 1);
	}
	
	/**
	 * 
	 * Check if a string is a Number
	 * 
	 * @param input
	 * @return boolean
	 */
	public static boolean isNumber(String input) {
		try {
	    Integer.parseInt(input);
	    return true;
		} catch (NumberFormatException e) {
	    System.out.println("Please write a number");
	    return false;
		}
	}

}
