package fr.remirubis.anthill.models;

import fr.remirubis.anthill.Color;

public enum AntTypes {
	
	QUEEN ("Queen", "Adult ants whose job is to lay larvae", Color.RED, 50, 1),
	MALE ("Male", "Adult and whose job is to fertilize the queen", Color.CYAN, 20, 30),
	WORKER ("Worker", "Adult and whose job is to protected and feed th anthill", Color.MAGENTA, 50, 20),
	LARVAE ("Larvae", "Non-adult ants", Color.GREEN, 10, 30);
	
	private String name;
	private String desc;
	private Color color;
	private int maxAge;
	private int maxRandomValue;

	AntTypes(String name, String desc, Color redBackground, int maxAge, int maxRandomValue) {
		this.name = name;
		this.desc = desc;
		this.color = redBackground;
		this.maxAge = maxAge;
		this.maxRandomValue = maxRandomValue;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public Color getColor() {
		return color;
	}

	public int getMaxAge() {
		return maxAge;
	}
	
	public int getMaxRandomValue() {
		return maxRandomValue;
	}

}
