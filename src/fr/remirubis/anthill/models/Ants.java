package fr.remirubis.anthill.models;

import fr.remirubis.anthill.Main;

public class Ants {
	
	private int rank;
	private int age;
	private AntTypes type;
	private AntTypes futureType;

	public Ants(int rank, int age, AntTypes type) {
		this.rank = rank;
		this.age = age;
		this.type = type;
		this.futureType = generateFutureType();
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public AntTypes getType() {
		return type;
	}

	public void setType(AntTypes type) {
		this.type = type;
	}

	public AntTypes getFutureType() {
		return futureType;
	}

	public void setFutureType(AntTypes futureType) {
		this.futureType = futureType;
	}

	/**
	 * 
	 * Generate future ants type randomly
	 * 
	 * @return AntTypes
	 */
	private AntTypes generateFutureType() {
		if (type == AntTypes.LARVAE) {
			int rand = Main.random(21);
			if(rand == 1) {
				return AntTypes.QUEEN;
			} else if (rand <= 3) {
				return AntTypes.MALE;
			}
			return AntTypes.WORKER;
		} else {
			return type;
		}
	}
	
	/**
	 * 
	 * Change type of ants if is age and kill older ants
	 * 
	 * @return Ants
	 */
	public Ants evolve() {
		int newAge = getAge() + 1;
		setAge(newAge);
  	if (newAge == getType().getMaxAge()) {
  		if (getType() == AntTypes.LARVAE) {
  			setType(getFutureType());
  		} else {
  			setAge(0);
  		}
  	}
		return this;
	}

}
