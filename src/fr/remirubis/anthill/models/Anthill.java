package fr.remirubis.anthill.models;

import java.io.FilterInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import fr.remirubis.anthill.Color;
import fr.remirubis.anthill.Main;

public class Anthill {
	
	private static Map<AntTypes, ArrayList<Ants>> allAnts = new HashMap<AntTypes, ArrayList<Ants>>();
	private static int rank = 1;
	private static int currentDay = 1;

	/**
	 * @param population
	 */
	public Anthill(Map<AntTypes, Integer> population) {
		System.out.println(Color.BLACK_BACKGROUND + "---> Anthill generation in progress... " + Color.RESET);
		generateAnthill(population);
		dayInput();
	}
	
	/**
	 * Generate population of Anthill and start at day 1
	 * 
	 * @param population
	 */
	private void generateAnthill(Map<AntTypes, Integer>  population) {
    for (AntTypes antType : population.keySet()) {
      int nbType = population.get(antType);
      ArrayList<Ants> typeAnts = new ArrayList<Ants>();
      
      for (int i = 0; i < nbType; i++) {
      	Ants newAnt = new Ants(rank, 1, antType);
      	typeAnts.add(newAnt);
      	rank++;
      }
      
      allAnts.put(antType, typeAnts);
    }
    currentDay++;
	}

	/**
	 * Methods who waiting an input like ENTER or a day in console
	 */
	public static void dayInput() {
		boolean error = true;
    String input = "";
    try (Scanner scan = new Scanner(new FilterInputStream(System.in){public void close(){}})) {
      while (error) {
      	System.out.print(Color.BLACK_BACKGROUND + "---> Write the number of days you wish to spend or press enter to go to the next day :" + Color.RESET + " ");
      	
      	if(!scan.hasNextLine() || (input = scan.nextLine().toLowerCase()).length() == 0) {
      		error = false;
      		newDay(1);
      	} else {        		
      		boolean isNb = Main.isNumber(input);
      		if (isNb) {
      			error = false;
      			newDay(Integer.parseInt(input));
      		}
      	}
      }
    }
    dayInput();
	}
	
	/**
	 * 
	 * Generate all ants if is needed and set older if exists
	 * 
	 * @param nb
	 */
	private static void newDay(int nb) {
		System.out.println(Color.BLACK_BACKGROUND + "-----------------------" + Color.RESET);
		System.out.println("Anthill age " + currentDay);
		
		int nbDead = 0;
		int nbBorn = 0;
		
		ArrayList<Ants> queens = new ArrayList<Ants>();
		ArrayList<Ants> workers = new ArrayList<Ants>();
		ArrayList<Ants> males = new ArrayList<Ants>();
		ArrayList<Ants> larvaes = new ArrayList<Ants>();
		
		for (AntTypes antType : allAnts.keySet()) {
      ArrayList<Ants> nbType = allAnts.get(antType);
      
      for (int i = 0; i < nbType.size(); i++) {
      	AntTypes oldType = nbType.get(i).getType();
      	Ants newAnt = nbType.get(i).evolve();
      	
      	if (newAnt.getAge() > 0) {
      		switch (newAnt.getType()) {
      		case QUEEN:
      			if (allAnts.get(AntTypes.MALE).size() >= 1 && oldType == newAnt.getType()) nbBorn += 10;
      			queens.add(newAnt);
      			break;
      		case WORKER:
      			workers.add(newAnt);
      			break;
      		case MALE:
      			males.add(newAnt);
      			break;
      		case LARVAE:
      			larvaes.add(newAnt);
      			break;
      		default:
      			throw new IllegalArgumentException("Unexpected type: " + newAnt.getType());
      		}
      	} else {
      		nbDead++;
      	}
      
      }
    }
		
		allAnts.put(AntTypes.QUEEN, queens);
		allAnts.put(AntTypes.WORKER, workers);
		allAnts.put(AntTypes.MALE, males);
		allAnts.put(AntTypes.LARVAE, larvaes);
		
		if (nbBorn > 0) newBorn(nbBorn);
		
		System.out.println("Amount of " + AntTypes.QUEEN.getColor() + AntTypes.QUEEN.getName() + Color.RESET + " : " + allAnts.get(AntTypes.QUEEN).size());
		System.out.println("Amount of " + AntTypes.WORKER.getColor() + AntTypes.WORKER.getName() + Color.RESET + " : " + allAnts.get(AntTypes.WORKER).size());
		System.out.println("Amount of " + AntTypes.MALE.getColor() + AntTypes.MALE.getName() + Color.RESET + " : " + allAnts.get(AntTypes.MALE).size());
		System.out.println("Amount of " + AntTypes.LARVAE.getColor() + AntTypes.LARVAE.getName() + Color.RESET + " : " + allAnts.get(AntTypes.LARVAE).size());
		System.out.println("New birth : " + nbBorn);
		System.out.println("Number of dead : " + nbDead);
		System.out.println(Color.BLACK_BACKGROUND + "-----------------------" + Color.RESET);
		
		currentDay++;
		if (nb - 1 > 0) newDay(nb - 1);
	}
	
	/**
	 * 
	 * Generate new ants if is needed
	 * 
	 * @param nb
	 */
	private static void newBorn(int nb) {  
		ArrayList<Ants> nbType = allAnts.get(AntTypes.LARVAE);
		Ants antBorn = new Ants(rank, 1, AntTypes.LARVAE);
		nbType.add(antBorn);
		rank++;
		allAnts.put(AntTypes.LARVAE, nbType);
		if (nb - 1 > 0) newBorn(nb-1);
	}

}
