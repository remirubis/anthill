# Anthill

Simulation software for studying the development of an anthill

## Context

We’re going to study the development of an anthill in an environment without predators. To do that, we’re going to develop simulation software with Java language.

## :heart: Usage

- [ ] Clone project
- [ ] Open it in your IDE (like Eclipse)
- [ ] Execute program
- [ ] Configure your anthill, Queens, Workers, Males, Larvaes
- [ ] Go to next day with enter or enter the number of days you wish to spend
- [ ] Leave program with CTRL + C on windows

## Default configuration

Ants types:
- Queen
`Life 50 days and each day produce 10 larvaes if minimum 1 male is alive`
- Male
`Life 20 days`
- Worker
`Life 50 days`
- Larvae
`Life 10 days and after become a queen (Probability: 1/20), a male (Probability: 2/20) or a worker (Probability: 17/20)`

## Contributors
:bust_in_silhouette: [Rémi RUBIS](https://gitlab.com/remirubis)
